# README for urbanSpectra Team Dev Dojo project #

![team-dev-dojo](images/dojo-logo.png)

This README.md file is specially formatted for readme docs using a language called [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

This project is stage one of three designed for expert workflow analytics and social learning

* Confused or stuck? Join this [Facebook Group](https://www.facebook.com/groups/OpenSourceSoftwareProjectMgtTalent/) and post a question.

* NO SKILL PREREQUISITES NEEDED!!
---
## STAGE I of III: SOLO LOCAL - Time to complete = 5 minutes

1. Download and install git locally.  Configure local git.  Install an [ascii editor]().

* We Support All Operating Systems

* [Apple OSX Crowd](readme/osx.md) 

* [Linux Crowd](readme/linux.md)

* [Windows Crowd](readme/win.md)

---
* SOLO LOCAL WORKFLOW DIAGRAM:
![solo local git workflow diagram](images/git-solo-local-workflow.svg).

* For an alternate diagram that attempts to get the same ideas across, [Read This](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository).

* Solo, Remote: Time to complete = 10 minutes

* Team, Remote: Time to complete = 15 minutes

* Team, [Advanced](http://www.gitready.com): Ongoing

### What is this repository used for? ###

* This repository is where devs new to git and github/bitbucket can get jumpstarted FOR FREE!
* Best way to do that is with a buddy to facilitate the collaborative nature of this kind team work.
* You will install and configure git locally,

---
### How do I get started with *Solo Local* ? ###

1. Find the [command-line window]().  Start to use [command history]().
2. Install git locally.
3. Start to review the first workflow diagram.  See if you can follow it.

* Consider what how that diagram might be improved.
---

##  Second solo local workflow diagram extends the one above.
4. Review [solo local git test-driven workflow diagram]().

5. Review [remote git basic commands]: clone, remote, pull, push, fetch, merge, rebase.

6. Start to use git locally with your daily work.

7. Learn to start a local git repo [and then move that to github.com]().

---
### What do we fix next in this repo?  ###
* [x]: Add the github public version as downstream remote.
* [ ]: Add two workflow diagrams:
    - [x]: Solo Local Git Workflow
    - [ ]: Solo Local Git Test-Driven Workflow
* [ ]: Push to github public and mess up and restore public team-dev-dojo repo from origin.
* [ ]: Clean up this page and add README.md formatting examples.
* [ ]: Review this page with a noob to ensure that all steps are clear.
* [ ]: Here we learn to screw it up, clean it up, and then prevent it going forwards as a team.
