# README for Team Dev Dojo Linux Users Setups #

![team-dev-dojo](../images/dojo-logo.png)


---
For OSX users,
to get to the command line, use the pre-installed app named "Terminal" which is under Applications/Utilities/Terminal.
![OSXTerminal](images/terminalApp.png)
---
* If you want to learn more about the OS X command-line Terminal app, then [Read This](https://en.wikipedia.org/wiki/Terminal_(macOS)).

* For Windows users, we recommend installing linux-like command line tools.
* NOTE: Windows users can use [scoop](http://scoop.sh) to install linux-like stuff.
* NOTE: Windows users also can consider using [gitForWindows](http://gitforwindows.org/), as it includes all kinds of other linuxy goodies.
---


[Return To Team Dev Dojo Repo Home](../README.md)