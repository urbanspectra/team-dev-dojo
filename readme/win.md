# README for Team Dev Dojo Linux Users Setups #

![team-dev-dojo](../images/dojo-logo.png)


---
## For OSX users, to get to the command line (CLI), use the pre-installed app named "Terminal" which is under Applications/Utilities/Terminal.
![OSXTerminal](images/terminalApp.png)
---
* If you want to learn more about the OS X command-line Terminal app, then [Read This](https://en.wikipedia.org/wiki/Terminal_(macOS)).
---
For Windows users, we recommend installing linux-like command line tools.

* The latest greatest for this is [Windows WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
* Windows users also can consider using [gitForWindows](http://gitforwindows.org/), as it includes all kinds of other linuxy goodies.
* Windows users can use [scoop](http://scoop.sh) to install linux-like stuff.
* There are also other options, like [VirtualBox](https://www.virtualbox.org/) running true Linux, as well as [Cygwin](https://www.cygwin.com/) linux emulations.
* There are also desktop apps which will do *some* things nicely, but we prefer command-line tools.
---
We will discuss the trade-offs for each of these options below.

- [Cygwin vs WSL](https://askubuntu.com/questions/813848/what-are-the-differences-between-windows-bash-and-cygwin)


[Return To Team Dev Dojo Repo Home](../README.md)

