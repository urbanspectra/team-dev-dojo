# README for Team Dev Dojo Linux Users Setups #

![team-dev-dojo](../images/dojo-logo.png)


---
For OSX users,
to get to the command line, use the pre-installed app named "Terminal" which is under Applications/Utilities/Terminal.
![OSXTerminal](images/terminalApp.png)
---
* If you want to learn more about the OS X command-line Terminal app, then [Read This](https://en.wikipedia.org/wiki/Terminal_(macOS)).

* For Windows users, we recommend installing linux-like command line tools.
* NOTE: Windows users can use [scoop](http://scoop.sh) to install linux-like stuff.
* NOTE: Windows users also can consider using [gitForWindows](http://gitforwindows.org/), as it includes all kinds of other linuxy goodies.
---


[Return To Team Dev Dojo Repo Home](../README.md)

1. [Install latest version of git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

* NOTE: OSX users use [homebrew](http://brew.sh).
* NOTE: Linux users, uninstall git 1.x and install git core 2.x (no dependencies).  Version 2.x is best for team dev.
* NOTE: Widows users can use [scoop](http://scoop.sh).
* NOTE: Windows users also can consider using [gitForWindows](http://gitforwindows.org/), as it includes all kinds of other linux goodies.

* Test git after installation and confirm it is version 2.x:
```
$ git version
```
* Install ascii code editor:
* * [Linux: Sublime Text](https://www.sublimetext.com/3)
* * [OSX: Sublime Text](https://www.sublimetext.com/3)
* * [Windows: NotePad++](https://notepad-plus-plus.org/download/)

2. [Configure local git](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup) with your full legal name and active email address.
```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```
* We recommend using [gmail](http://www.gmail.com) which comes free with [google docs]().

3. Learn to use [local git basic commands]():  status, init, add, commit, log, diff, diff-cached, branch, merge, rebase.
* Use git help to learn how to reference git subcommand support info on command-line.
```
$ git config --help
```
or
```
$ git add --help
```

- Hit space to page thru.
- Type q to exit help.

